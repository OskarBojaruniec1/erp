package bookkeeping.contractor;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ContractorValidatorsTest {

    ContractorValidators cv;

    @BeforeEach
    void setUp(){
        cv = new ContractorValidators();
    }

    @Test
    void forLessThan10digitsShouldReturnFalse(){
        Assertions.assertThat(cv.checkIsNIPCorrect("123456789")).isFalse();
    }

    @Test
    void forMoreThan10digitsShouldReturnFalse(){
        Assertions.assertThat(cv.checkIsNIPCorrect("12345123456123456")).isFalse();
    }

    @Test
    void for10digitsShouldReturnTrue(){
        Assertions.assertThat(cv.checkIsNIPCorrect("8522384067")).isTrue();
    }

}