package bookkeeping.finances;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class TaxesCalculatorTest {

    TaxesCalculator tc;

    @BeforeEach
    void setUp(){
        tc = new TaxesCalculator();
    }

    @Test
    void shouldReturn0forIncomeLessThan30_001(){
        org.assertj.core.api.Assertions.assertThat(tc.calculateTax(30000)).isEqualTo(0);
    }
    @Test
    void shouldReturnMoreThan0ForIncomeHigherThan30_000(){
        org.assertj.core.api.Assertions.assertThat(tc.calculateTax(30001)).isNotEqualTo(0);
    }

    @ParameterizedTest
    @CsvSource({
            "111_000",
            "40_000",
            "150_000"
    })
    void shouldReturnTaxesCalculatedInFirstWayForIncomeLessThan120_000(double income){
        double incomeWithTaxFree = income - 30_000;
        org.assertj.core.api.Assertions.assertThat(tc.calculateTax(income))
                .isEqualTo(incomeWithTaxFree * 0.17);
    }
    @Test
    void shouldReturnTaxesCalculatedInSecondWayForIncomeHigherThan120_000(){

        org.assertj.core.api.Assertions.assertThat(tc.calculateTax(160_000)).isEqualTo(23600);
    }

}