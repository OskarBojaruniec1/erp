package warehouse.production.recipes;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class RecipeValidatorsTest {

    @ParameterizedTest
    @CsvSource({
            "1 days",
            "1456 years",
            "31 months"

    })
    void shouldReturnTrueForCorrectInputs(String timeOfProduction){

        RecipeValidators rv = new RecipeValidators();
        Assertions.assertThat(rv.checkIsTimeOfProductionHasCorrectFormat(timeOfProduction)).isTrue();

    }

    @ParameterizedTest
    @CsvSource({
            "1days",
            "444  years",
            "48 mnths"
    })
    void shouldReturnFalseForIncorrectInput(String timeOfProduction) {

        RecipeValidators rv = new RecipeValidators();
        Assertions.assertThat(rv.checkIsTimeOfProductionHasCorrectFormat(timeOfProduction)).isFalse();

    }
}