package address;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class AddressValidatorsTest {

    @ParameterizedTest
    @CsvSource({
            "7one-244",
            "3-11",
            "zi-pcd"
    })
    void shouldReturnFalseForIncorrectZipCodes(String zipcode){

        AddressValidators av = new AddressValidators();

        Assertions.assertThat(av.checkIsZipCodeCorrect(zipcode)).isFalse();
    }

    @ParameterizedTest
    @CsvSource({
            "71-244",
            "31-243",
            "48-232"
    })
    void shouldReturnTrueForCorrectZipCodes(String zipcode){

        AddressValidators av = new AddressValidators();

        Assertions.assertThat(av.checkIsZipCodeCorrect(zipcode)).isTrue();
    }

}