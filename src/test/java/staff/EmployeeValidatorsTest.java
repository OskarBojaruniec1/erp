package staff;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EmployeeValidatorsTest {

    EmployeeValidators ev;

    @BeforeEach
    void setUp(){
        ev = new EmployeeValidators();
    }

    @Test
    void forLessThan11DigitsShouldReturnFalse(){
        org.assertj.core.api.Assertions.assertThat(ev.checkIsPeselCorrect("1314")).isFalse();
    }

    @Test
    void forMoreThan11DigitsShouldReturnFalse(){
        Assertions.assertThat(ev.checkIsPeselCorrect("319393931933113")).isFalse();
    }

    @Test
    void for11DigitsShouldReturnTrue(){
        Assertions.assertThat(ev.checkIsPeselCorrect("12345678910")).isTrue();
    }

}