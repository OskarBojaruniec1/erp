package shop;

import bookkeeping.invoices.InvoiceOperations;
import shop.cart.Cart;
import shop.cart.Item;
import shop.cart.ItemOperations;
import warehouse.materials.Material;
import warehouse.materials.MaterialValidators;
import warehouse.materials.MaterialsOperations;
import warehouse.production.Production;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Shop {

    InvoiceOperations io = new InvoiceOperations();

    MaterialsOperations mo = new MaterialsOperations();

    MaterialValidators mv = new MaterialValidators();

    ItemOperations itemOps = new ItemOperations();

    Scanner sc = new Scanner(System.in);

    public void buyMaterials() {

        System.out.println("Enter id of material you want to buy");
        System.out.println("If it is new material add it to warehouse database");
        System.out.println("1 - Enter ID\n" +
                "2 - Add to warehouse");

        String choose = sc.next();

        switch (choose) {
            case "1" -> {
                Material material = mo.getById();
                System.out.println("Cost is " + material.getPriceToBuy() + " per " + material.getUnit());
                System.out.println("Enter how many " + material.getUnit() + " you would like to buy");
                double amount = sc.nextDouble();
                mo.updateAmountOfMaterial(material.getId(), amount, "add");
                Item item = new Item(material.getName(), amount, material.getPriceToBuy() * amount);
                itemOps.saveItemInDataBase(item);
            }
            case "2" -> {
                mo.addNewMaterial();
                buyMaterials();
            }
            default -> System.out.println("Unknown command");
        }
    }

    public void sellMaterials() {

        List<Item> listOfItems = new ArrayList<>();
        boolean isAllAdded = false;

        while (!isAllAdded) {

            Material material = mo.getById();

            while (!mv.checkIsMaterialExists(material)) {
                System.out.println("Material not found");
                material = mo.getById();
            }
            System.out.println(material);

            System.out.println("Enter amount to sell");
            double amount = sc.nextDouble();

            while (!mv.checkQuantityOfMaterialInWarehouse(material.getId(),
                    amount)) {

                System.out.println("Want to buy this material?");
                System.out.println("1 - Yes\n" +
                        "2 - No");

                String choose = sc.next();

                if (choose.equals("1")) buyMaterials();
                System.out.println(material);

                System.out.println("Enter amount to sell");
                amount = sc.nextDouble();
            }

            Item item = new Item(material.getName(), amount, material.getPriceToSell() * amount);
            listOfItems.add(item);

            System.out.println("Want to add something else?");
            System.out.println("1 - Yes\n" +
                    "2 - No");

            String choose = sc.next();

            if (!choose.equals("1")) isAllAdded = true;

            mo.updateAmountOfMaterial(material.getId(), amount, "delete");
        }

        Cart cart = new Cart(listOfItems);

        io.createInvoice(cart, calculateFullCost(cart));
    }

    public void sellProduct() {

        Production production = new Production();
        Cart cart = production.sellProduct();

        if (cart == null){
            System.out.println("Want to buy this material?");
            System.out.println("1 - Yes\n" +
                    "2 - No");

            String choose = sc.nextLine();
            switch (choose){

                case "1" -> buyMaterials();
                case "2" -> {
                    return;
                }
                default -> System.out.println("Unknown command");
            }
        }

        assert cart != null;
        io.createInvoice(cart, calculateFullCost(cart));
    }

    private double calculateFullCost(Cart cart) {

        double fullCost = 0;


        for (Item i : cart.getListOfItems()) {

            fullCost += i.getCost();
        }

        return fullCost;
    }
}
