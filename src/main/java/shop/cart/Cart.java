package shop.cart;

import com.opencsv.bean.CsvBindAndSplitByName;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Cart extends TextToItem {

    @CsvBindAndSplitByName(elementType = Item.class, splitOn = "\\|", converter = TextToItem.class)
    private List<Item> listOfItems;

    public String cartItemsAsString() {

        StringBuilder sb = new StringBuilder();
        int counter = 0;

        for (Item i : listOfItems) {

            counter++;

            sb.append(convertToWrite(i));
            if (counter == listOfItems.size()) break;
            sb.append('|');
        }

        return String.valueOf(sb);
    }
}
