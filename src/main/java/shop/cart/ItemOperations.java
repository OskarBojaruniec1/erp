package shop.cart;

import CRUD.CRUD;
import lombok.Getter;

import java.io.File;

@Getter
public class ItemOperations extends CRUD<Item> {

    private final File itemsDB = new File("src/main/resources/boughtItems.csv");

    public void saveItemInDataBase(Item item) {

        String[] attributesOfClass = {item.getName(), String.valueOf(item.getAmount()), String.valueOf(item.getCost())};

        save(itemsDB, attributesOfClass);
    }
}
