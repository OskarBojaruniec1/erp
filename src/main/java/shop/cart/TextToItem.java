package shop.cart;

import com.opencsv.bean.AbstractCsvConverter;

public class TextToItem extends AbstractCsvConverter {

    @Override
    public Object convertToRead(String s) {

        Item item = new Item();
        String[] split = s.split(";", 3);
        item.setName(split[0]);
        item.setAmount(Double.parseDouble(split[1]));
        item.setCost(Double.parseDouble(split[2]));
        return item;

    }

    @Override
    public String convertToWrite(Object value) {

        Item item = (Item) value;
        return String.format("%s;%s;%s", item.getName(), item.getAmount(), item.getCost());
    }
}
