package address;

import com.opencsv.bean.CsvBindByName;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Address {

    @CsvBindByName
    private String street;
    @CsvBindByName
    private String numberOfStreet;
    @CsvBindByName
    private String zipCode;

    public String[] attributesOfClassAddress() {
        return new String[]{street, numberOfStreet, zipCode};
    }
}
