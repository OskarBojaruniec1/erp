package address;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddressValidators {

    public boolean checkIsZipCodeCorrect(String zipCode) {

        String regex = "\\d{2}-\\d{3}";

        Pattern compiledPattern = Pattern.compile(regex);
        Matcher matcher = compiledPattern.matcher(zipCode);

        if (matcher.matches()) {
            return true;
        } else {
            System.out.println("Zip code is incorrect");
            return false;
        }
    }
}
