package address;

import java.util.Scanner;

public class AddressOperations {

    AddressValidators av = new AddressValidators();

    public Address addAddress() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter street");
        String street = sc.next();

        System.out.println("Enter number of street");
        String numberOfStreet = sc.next();

        System.out.println("Enter zip code");
        String zipCode = sc.next();

        while (!av.checkIsZipCodeCorrect(zipCode)) {

            System.out.println("Enter zip code");
            zipCode = sc.next();
        }

        return new Address(street, numberOfStreet, zipCode);
    }

    public Address updateAddress(Address address) {

        Scanner sc = new Scanner(System.in);

        System.out.println(address);
        System.out.println("1 - Street\n" +
                "2 - Number of street\n" +
                "3 - Zip code");

        String choose = sc.next();

        switch (choose) {

            case "1" -> {
                System.out.println("Enter new street");
                address.setStreet(sc.next());
            }
            case "2" -> {
                System.out.println("Enter new number of street");
                address.setNumberOfStreet(sc.next());
            }
            case "3" -> {
                System.out.println("Enter new zip code");
                String zipCode = sc.next();

                while (!av.checkIsZipCodeCorrect(zipCode)) {

                    System.out.println("Enter zip code");
                    zipCode = sc.next();
                }

                address.setZipCode(zipCode);
            }
            default -> System.out.println("Unknown command");
        }
        return address;
    }
}
