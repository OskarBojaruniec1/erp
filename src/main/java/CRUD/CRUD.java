package CRUD;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.exceptions.CsvValidationException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Scanner;

public abstract class CRUD<T> {


    protected void save(File file, String[] attributesOfClass) {

        FileWriter outputfile;


        try {
            outputfile = new FileWriter(file, true);

            CSVWriter writer = new CSVWriter(outputfile);

            writer.writeNext(attributesOfClass, false);

            writer.close();

        } catch (IOException e) {
            System.out.println("Pies zapis");
        }


    }

    public void deleteById(long id, File file) {
        updateAndDeleteOperations("Delete", id, file, null);
    }

    public List<T> findAll(File file, Class<T> clazz) {

        Reader reader;

        try {
            reader = Files.newBufferedReader(Path.of(file.getPath()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return new CsvToBeanBuilder<T>(reader)
                .withType(clazz)
                .build()
                .parse();

    }

    public T findById(long id, File file, Class<T> clazz) {

        List<T> objects = findAll(file, clazz);
        String idToString = Long.toString(id);

        for (Object x : objects) {
            if (x.toString().contains("id=" + idToString)) return clazz.cast(x);
        }

        return null;
    }

    protected void update(long id, File file, String[] attributesOfClass) {
        updateAndDeleteOperations("Update", id, file, attributesOfClass);
    }

    private void updateAndDeleteOperations(String choose, long id, File file, String[] attributesOfClass) {

        File tempFile = new File(file.getPath() + "temp");

        CSVReader reader;

        try {
            reader = new CSVReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        String[] line;
        String idToString = Long.toString(id);

        boolean isIdFound = false;

        while (true) {

            try {
                if ((line = reader.readNext()) == null) break;
            } catch (IOException | CsvValidationException e) {
                throw new RuntimeException(e);
            }

            if (idToString.equals(line[0])) {

                isIdFound = true;

                if (choose.equals("Update")) {
                    save(tempFile, attributesOfClass);
                }
                continue;
            }
            save(tempFile, line);
        }

        try {
            reader.close();
        } catch (IOException e) {
            System.out.println("pies");
        }


        boolean successfulDelete = file.delete();
        boolean successfulRename = tempFile.renameTo(file);

        if (!isIdFound) {
            System.out.println("Couldn't find entered ID");
            return;
        }
        if (successfulDelete && successfulRename) System.out.println(choose + " is successful");
    }

    protected long assignID(File file) {

        Scanner scFile;

        try {
            scFile = new Scanner(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        long id = 1;

        scFile.nextLine();

        while (scFile.hasNextLine()) {

            String line = scFile.nextLine();
            String[] splitLine = line.split(",");

            id = Long.parseLong(splitLine[0]) + 1;
        }

        return id;
    }
}