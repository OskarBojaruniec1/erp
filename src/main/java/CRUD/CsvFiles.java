package CRUD;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CsvFiles extends CRUD<CsvFiles>  {


    public void createCsvFiles() {

        HashMap<File,String[]> csvFilesMap = new HashMap<>();

        File usersDB = new File("src/main/resources/users.csv");
        String[] usersDBHeaders = {"id", "login", "password"};
        csvFilesMap.put(usersDB,usersDBHeaders);
        
        File contractorsDB = new File("src/main/resources/contractors.csv");
        String[] contractorsDBHeaders = {"id","name","nip","street","numberOfStreet","zipCode"};
        csvFilesMap.put(contractorsDB,contractorsDBHeaders);
        
        File invoicesDB = new File("src/main/resources/invoices.csv");
        String[] invoicesDBHeaders = {"id","amount","currency","paymentWay","contractorId","listOfItems"};
        csvFilesMap.put(invoicesDB,invoicesDBHeaders);
        
        File itemsDB = new File("src/main/resources/boughtItems.csv");
        String[] itemsDBHeaders = {"name","amount","cost"};
        csvFilesMap.put(itemsDB,itemsDBHeaders);
        
        File warehouseDB = new File("src/main/resources/warehouse.csv");
        String[] warehouseDBHeaders = {"id","name","quantity","unit","priceToBuy","priceToSell"};
        csvFilesMap.put(warehouseDB,warehouseDBHeaders);
        
        File recipesDB = new File("src/main/resources/recipes.csv");
        String[] recipesDBHeaders = {"id","name","unit","timeOfProduction","costOfProduction","listOfComponents"};
        csvFilesMap.put(recipesDB,recipesDBHeaders);
        
        File employeesDB = new File("src/main/resources/employees.csv");
        String[] employeesDBHeaders = {"id","name","surname","pesel","salary","street","numberOfStreet","zipCode"};
        csvFilesMap.put(employeesDB,employeesDBHeaders);

        for (Map.Entry<File,String[]> set: csvFilesMap.entrySet()) {

            try {
                boolean isCreated = set.getKey().createNewFile();
                System.out.printf("%s is created properly",set.getKey());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            save(set.getKey(),set.getValue());

        }

    }
}
