package staff;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmployeeValidators {

    public boolean checkIsPeselCorrect(String pesel){

        String regex = "\\d{11}";

        Pattern compiledPattern = Pattern.compile(regex);
        Matcher matcher = compiledPattern.matcher(pesel);

        if (matcher.matches()) {
            return true;
        } else {
            System.out.println("Pesel is incorrect");
            return false;
        }
    }
}
