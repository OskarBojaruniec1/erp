package staff;

import CRUD.CRUD;
import address.AddressOperations;
import lombok.Getter;

import java.io.File;
import java.util.Scanner;

@Getter
public class EmployeeOperations extends CRUD<Employee> {

    private final File employeesDB = new File("src/main/resources/employees.csv");

    AddressOperations addressOps = new AddressOperations();

    EmployeeValidators ev = new EmployeeValidators();

    Scanner sc = new Scanner(System.in);

    public void createEmployee() {

        System.out.println("Enter name");
        String name = sc.next();

        System.out.println("Enter surname");
        String surname = sc.next();

        System.out.println("Enter pesel");
        String pesel = sc.next();

        while (!ev.checkIsPeselCorrect(pesel)) {
            System.out.println("Enter pesel");
            pesel = sc.next();
        }

        System.out.println("Enter monthly salary");
        double salary = sc.nextDouble();

        long id = assignID(employeesDB);

        Employee e = new Employee(id, name, surname, pesel, salary, addressOps.addAddress());

        save(employeesDB, e.attributesOfClass());
    }

    private long getIdFromUser() {

        System.out.println("Enter ID");
        long id = sc.nextLong();
        sc.nextLine();

        return id;
    }

    public Employee getById() {

        return findById(getIdFromUser(), employeesDB, Employee.class);
    }

    public void deleteById() {

        deleteById(getIdFromUser(), employeesDB);
    }

    public void updateImplementation() {


        System.out.println("Enter id of Employee to change");
        long id = sc.nextLong();
        sc.nextLine();

        Employee employeeToChange = findById(id, employeesDB, Employee.class);

        System.out.println("What would you like to change");
        System.out.println("1 - Name\n" +
                "2 - Surname\n" +
                "3 - Pesel\n" +
                "4 - Salary\n" +
                "5 - Address");

        String choose = sc.next();

        switch (choose) {
            case "1" -> {
                System.out.println("Enter new name");
                employeeToChange.setName(sc.next());
            }
            case "2" -> {
                System.out.println("Enter new surname");
                employeeToChange.setSurname(sc.next());
            }
            case "3" -> {
                System.out.println("Edit pesel");
                String pesel = sc.next();

                while (!ev.checkIsPeselCorrect(pesel)) {
                    System.out.println("Enter pesel");
                    pesel = sc.next();
                }

                employeeToChange.setPesel(pesel);
            }
            case "4" -> {
                System.out.println("Enter new salary");
                employeeToChange.setSalary(sc.nextDouble());
            }
            case "5" -> employeeToChange.setAddress(addressOps.updateAddress(employeeToChange.getAddress()));

            default -> System.out.println("Unknown command");
        }

        update(id, employeesDB, employeeToChange.attributesOfClass());
    }
}
