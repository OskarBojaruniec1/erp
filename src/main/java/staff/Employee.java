package staff;

import address.Address;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvRecurse;
import lombok.*;
import org.apache.commons.lang3.ArrayUtils;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Employee {

    @CsvBindByName
    private long id;
    @CsvBindByName
    private String name;
    @CsvBindByName
    private String surname;
    @CsvBindByName
    private String pesel;
    @CsvBindByName
    private double salary;
    @CsvRecurse
    private Address address;

    protected String[] attributesOfClass() {

        String[] attributes = {Long.toString(id), name, surname,pesel, String.valueOf(salary)};

        return ArrayUtils.addAll(attributes, address.attributesOfClassAddress());

    }

}
