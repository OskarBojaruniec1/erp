package currenciesRatesApi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Client {

    HttpClient client = HttpClient.newBuilder().build();

    public HttpResponse<String> call(String currencyCode, String date) {

        HttpRequest request = getHttpRequest(currencyCode, date);

        try {
            return client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }

    }

    private HttpRequest getHttpRequest(String currencyCode, String date) {

        String BASE_URL = "http://api.nbp.pl/api/exchangerates/rates/c/";
        String JSON = "?format=json";

        return HttpRequest.newBuilder()
                .uri(URI.create(BASE_URL + currencyCode + "/" + date + "/" + JSON))
                .GET()
                .build();
    }

    public CurrencyModel jsonToObject(String currencyCode, String date) {

        String body = call(currencyCode, date).body();

        ObjectMapper om = new ObjectMapper();

        try {
            return om.readValue(body, CurrencyModel.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }
}
