package currenciesRatesApi;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class CurrencyModel {

    private String table;
    private String currency;
    private String code;
    private List<Rate> rates;
}
