package currenciesRatesApi;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Rate {

    private String no;
    private String effectiveDate;
    private double bid;
    private double ask;
}
