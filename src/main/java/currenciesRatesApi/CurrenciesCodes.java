package currenciesRatesApi;

import lombok.Getter;

@Getter
public enum CurrenciesCodes {

    USD("usd"),
    EUR("eur"),
    PLN("pln");

    final String code;

    CurrenciesCodes(String code) {
        this.code = code;
    }
}
