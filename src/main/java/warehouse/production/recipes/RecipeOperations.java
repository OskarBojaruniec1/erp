package warehouse.production.recipes;

import CRUD.CRUD;
import warehouse.materials.Material;
import warehouse.materials.MaterialUnits;
import warehouse.materials.MaterialsOperations;
import warehouse.production.components.Component;
import warehouse.production.components.ComponentOperations;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class RecipeOperations extends CRUD<Recipe> {

    private final File recipesDB = new File("src/main/resources/recipes.csv");
    Scanner sc = new Scanner(System.in);

    ComponentOperations co = new ComponentOperations();

    RecipeValidators rv = new RecipeValidators();

    public File getRecipesDB() {
        return recipesDB;
    }

    public void createNewRecipe() {

        boolean isAllAdded = false;
        List<Component> components = new ArrayList<>();

        System.out.println("Enter name of new product");
        String name = sc.next();

        System.out.println("Enter unit of product\n" + Arrays.toString(MaterialUnits.values()));
        MaterialUnits unit = MaterialUnits.valueOf(sc.next());

        while (!isAllAdded) {

            Component component = co.createNewComponent();

            components.add(component);

            System.out.println(components);

            System.out.println("Want to add something else?");
            System.out.println("1 - Yes\n" +
                    "2 - No");

            String choose = sc.next();

            if (!choose.equals("1")) isAllAdded = true;
        }

        sc.nextLine();
        System.out.println("Enter time of production this product");
        System.out.println("Acceptable formats (x hours, x days, x years)");

        String timeOfProduction = sc.nextLine();
        while(!rv.checkIsTimeOfProductionHasCorrectFormat(timeOfProduction)){

            System.out.println("Enter time of production this product");
            timeOfProduction = sc.nextLine();
        }

        double costOfProduction = calculateCostOfProduction(components);

        long id = assignID(recipesDB);

        Recipe recipe = new Recipe(id, name, unit, timeOfProduction, costOfProduction, components);

        save(recipesDB, recipe.attributesOfClassRecipe());
    }

    private double calculateCostOfProduction(List<Component> components) {

        MaterialsOperations mo = new MaterialsOperations();

        double costOfProduction = 0;

        for (Component c : components) {

            Material material = mo.findById(c.getMaterialID(), mo.getWarehouseDB(), Material.class);
            costOfProduction += material.getPriceToBuy();
        }

        return costOfProduction;
    }

    private long getIdFromUser() {

        System.out.println("Enter ID of Recipe");
        long id = sc.nextLong();
        sc.nextLine();

        return id;
    }

    public Recipe getById() {

        return findById(getIdFromUser(), recipesDB, Recipe.class);
    }

    public void deleteById() {

        deleteById(getIdFromUser(), recipesDB);
    }

    public void updateImplementation() {

        Recipe recipeToChange = getById();

        System.out.println("What would you like to change");
        System.out.println("1 - Name\n" +
                "2 - Unit\n" +
                "3 - timeOfProduction\n");

        String choose = sc.next();

        switch (choose) {
            case "1" -> {
                System.out.println("Enter new name");
                recipeToChange.setName(sc.next());
            }
            case "2" -> {
                System.out.println("Enter new unit");
                System.out.println(Arrays.toString(MaterialUnits.values()));
                recipeToChange.setUnit(MaterialUnits.valueOf(sc.next()));
            }
            case "3" -> {
                System.out.println("Enter new time of production");
                recipeToChange.setTimeOfProduction(sc.next());
            }

            default -> System.out.println("Unknown command");
        }
        update(recipeToChange.getId(), recipesDB, recipeToChange.attributesOfClassRecipe());
    }
}
