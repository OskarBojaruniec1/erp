package warehouse.production.recipes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RecipeValidators {

    public boolean checkIsTimeOfProductionHasCorrectFormat(String timeOfProduction){

        Pattern compiledPattern = Pattern.compile("\\d+\\s(months|days|years)");
        Matcher matcher = compiledPattern.matcher(timeOfProduction);

        if (matcher.matches()) {
            return true;
        } else {
            System.out.println("Incorrect time of production");
            return false;
        }
    }

    public boolean checkIsRecipeExists(Recipe recipe){
        return recipe != null;
    }
}
