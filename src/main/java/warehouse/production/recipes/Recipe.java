package warehouse.production.recipes;

import com.opencsv.bean.CsvBindAndSplitByName;
import com.opencsv.bean.CsvBindByName;
import lombok.*;
import warehouse.materials.MaterialUnits;
import warehouse.production.components.Component;
import warehouse.production.components.TextToComponent;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Recipe extends TextToComponent {

    @CsvBindByName
    private long id;
    @CsvBindByName
    private String name;
    @CsvBindByName
    private MaterialUnits unit;
    @CsvBindByName
    private String timeOfProduction;
    @CsvBindByName
    private double costOfProduction;
    @CsvBindAndSplitByName(elementType = Component.class, splitOn = "\\|", converter = TextToComponent.class)
    private List<Component> listOfComponents;

    public String[] attributesOfClassRecipe() {

        StringBuilder sb = new StringBuilder();
        int counter = 0;


        for (Component c : listOfComponents) {

            counter++;

            sb.append(convertToWrite(c));
            if (counter == listOfComponents.size()) break;
            sb.append('|');
        }

        return new String[]{Long.toString(id),
                name,
                String.valueOf(unit),
                timeOfProduction,
                Double.toString(costOfProduction),
                String.valueOf(sb)
        };
    }
}

