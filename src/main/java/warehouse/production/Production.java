package warehouse.production;

import CRUD.CRUD;
import menus.warehouseMenus.WarehouseMenu;
import shop.cart.Cart;
import shop.cart.Item;
import warehouse.materials.MaterialValidators;
import warehouse.materials.MaterialsOperations;
import warehouse.production.components.Component;
import warehouse.production.recipes.Recipe;
import warehouse.production.recipes.RecipeOperations;
import warehouse.production.recipes.RecipeValidators;

import java.util.*;

public class Production extends CRUD<Recipe> {

    Scanner sc = new Scanner(System.in);

    MaterialsOperations mo = new MaterialsOperations();

    MaterialValidators mv = new MaterialValidators();

    RecipeValidators rv = new RecipeValidators();


    public Cart sellProduct() {

        RecipeOperations ro = new RecipeOperations();
        boolean inMethod = true;
        List<Item> items = new ArrayList<>();

        while (inMethod) {

            System.out.println("Enter ID of recipe to product it");
            long id = sc.nextLong();
            sc.nextLine();

            Recipe recipe = ro.findById(id, ro.getRecipesDB(), Recipe.class);
            if (!rv.checkIsRecipeExists(recipe)) {

                System.out.println("Recipe doesn't exists");
                System.out.println("Want to add new Recipe??");
                System.out.println("1 - Yes\n" +
                        "2 - No");

                String choose = sc.next();

                switch (choose){
                    case "1" -> ro.createNewRecipe();
                    case "2" -> WarehouseMenu.executeUserOption();
                }
            }

            System.out.println(recipe);

            System.out.println("Enter amount of products you want to sell");
            int amountOfProduct = sc.nextInt();

            double cost = recipe.getCostOfProduction() * amountOfProduct;

            items.add(new Item(recipe.getName(), amountOfProduct, cost));

            System.out.println("Want to add something else?");
            System.out.println("1 - Yes\n" +
                    "2 - No");

            String choose = sc.next();

            if (!choose.equals("1")) inMethod = false;

            if (!checkIsAllMaterialsAreInWarehouse(recipe.getListOfComponents(), amountOfProduct))
                return null;

            removeComponentsMaterials(recipe.getListOfComponents(), amountOfProduct);
        }

        return new Cart(items);
    }

    private void removeComponentsMaterials(List<Component> componentList, int amountOfProduct) {

        for (Component c : componentList) {

            mo.updateAmountOfMaterial(c.getMaterialID(),
                    c.getAmountOfMaterial() * amountOfProduct,
                    "delete");
        }
    }


    private boolean checkIsAllMaterialsAreInWarehouse(List<Component> componentList, int amountOfProduct) {

        HashMap<Long, Double> mapOfMaterials = new HashMap<>();

        for (Component c : componentList) {

            if (mapOfMaterials.containsKey(c.getMaterialID())) {

                mapOfMaterials.replace(c.getMaterialID(),
                        mapOfMaterials.get(c.getMaterialID()) +
                                c.getAmountOfMaterial() * amountOfProduct);

            } else mapOfMaterials.put(c.getMaterialID(), c.getAmountOfMaterial() * amountOfProduct);

        }

        for (Map.Entry<Long, Double> entry : mapOfMaterials.entrySet()) {

            if (!mv.checkQuantityOfMaterialInWarehouse(entry.getKey(),
                    entry.getValue())) return false;

        }
        return true;
    }
}
