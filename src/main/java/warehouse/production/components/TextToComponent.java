package warehouse.production.components;

import com.opencsv.bean.AbstractCsvConverter;

public class TextToComponent extends AbstractCsvConverter {

    @Override
    public Object convertToRead(String s) {

        Component component = new Component();
        String[] split = s.split(";", 2);
        component.setMaterialID(Long.parseLong(split[0]));
        component.setAmountOfMaterial(Double.parseDouble(split[1]));
        return component;

    }

    @Override
    public String convertToWrite(Object value) {

        Component component = (Component) value;
        return String.format("%s;%s", component.getMaterialID(), component.getAmountOfMaterial());
    }
}
