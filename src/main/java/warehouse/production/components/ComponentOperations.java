package warehouse.production.components;

import warehouse.materials.Material;
import warehouse.materials.MaterialValidators;
import warehouse.materials.MaterialsOperations;

import java.util.Scanner;

public class ComponentOperations {

    MaterialsOperations mo = new MaterialsOperations();

    Scanner sc = new Scanner(System.in);

    MaterialValidators mv = new MaterialValidators();


    public Component createNewComponent() {

        Material material = mo.getById();

        while (!mv.checkIsMaterialExists(material)) {
            System.out.println("Material not found");
            material = mo.getById();
        }

        System.out.println(material);

        System.out.println("Set amount of material to use");
        int amountOfMaterial = sc.nextInt();

        if (amountOfMaterial < 0) amountOfMaterial = 0;

        return new Component(material.getId(), amountOfMaterial);
    }
}
