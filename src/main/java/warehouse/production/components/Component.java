package warehouse.production.components;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Component extends TextToComponent {

    private long materialID;

    private double amountOfMaterial;

}
