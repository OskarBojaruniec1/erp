package warehouse.materials;

import CRUD.CRUD;
import lombok.Getter;

import java.io.File;
import java.util.Arrays;
import java.util.Scanner;

@Getter
public class MaterialsOperations extends CRUD<Material> {

    private final File warehouseDB = new File("src/main/resources/warehouse.csv");

    Scanner sc = new Scanner(System.in);


    public void addNewMaterial() {

        System.out.println("Enter name of material");
        String name = sc.next();

        System.out.println("Enter unit\n" + Arrays.toString(MaterialUnits.values()));
        MaterialUnits unit = MaterialUnits.valueOf(sc.next());

        double quantity = 0;

        System.out.println("Enter price to buy this material");
        double priceToBuy = sc.nextDouble();

        System.out.println("Enter price to sell in PLN");
        double priceToSell = sc.nextDouble();

        long id = assignID(warehouseDB);

        Material material = new Material(id, name, quantity, unit, priceToBuy, priceToSell);

        save(warehouseDB, material.attributesOfClassMaterial());

    }

    private long getIdFromUser() {

        System.out.println("Enter ID of material");
        long id = sc.nextLong();
        sc.nextLine();

        return id;
    }

    public Material getById() {

        return findById(getIdFromUser(), warehouseDB, Material.class);
    }

    public void deleteById() {

        deleteById(getIdFromUser(), warehouseDB);
    }

    public void updateImplementation() {

        System.out.println("Enter id of material to change");
        long id = sc.nextLong();
        sc.nextLine();

        Material materialToChange = findById(id, warehouseDB, Material.class);

        System.out.println("What would you like to change");
        System.out.println("1 - Name\n" +
                "2 - Unit\n" +
                "3 - Price to buy\n" +
                "4 - Price to sell");

        String choose = sc.next();

        switch (choose) {
            case "1" -> {
                System.out.println("Enter new name");
                materialToChange.setName(sc.next());
            }
            case "2" -> {
                System.out.println("Enter new unit");
                System.out.println(Arrays.toString(MaterialUnits.values()));
                materialToChange.setUnit(MaterialUnits.valueOf(sc.next()));
            }
            case "3" -> {
                System.out.println("Price to buy");
                materialToChange.setPriceToBuy(sc.nextDouble());
            }
            case "4" -> {
                System.out.println("Enter new priceToSell");
                materialToChange.setPriceToSell(sc.nextDouble());
            }
            default -> System.out.println("Unknown command");
        }
        update(id, warehouseDB, materialToChange.attributesOfClassMaterial());
    }

    public void updateAmountOfMaterial(long id, double amount, String choose) {

        Material m = findById(id, warehouseDB, Material.class);

        double actualQuantity = m.getQuantity();

        if(choose.equals("delete")) m.setQuantity(actualQuantity - amount);
        else m.setQuantity(actualQuantity + amount);

        update(id, warehouseDB, m.attributesOfClassMaterial());

    }
}
