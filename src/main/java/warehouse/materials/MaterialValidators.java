package warehouse.materials;

import CRUD.CRUD;

public class MaterialValidators extends CRUD<Material> {

    MaterialsOperations mo = new MaterialsOperations();



    public boolean checkQuantityOfMaterialInWarehouse(long id, double quantity) {

        Material material = findById(id, mo.getWarehouseDB(), Material.class);

        if (material.getQuantity() - quantity > 0) return true;
        else {
            System.out.println("You don't have that much of " + material.getName());

            return false;
        }
    }

        public boolean checkIsMaterialExists(Material material){
        return material != null;
    }
}
