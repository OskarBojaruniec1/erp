package warehouse.materials;

import com.opencsv.bean.CsvBindByName;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Material {

    @CsvBindByName
    private long id;
    @CsvBindByName
    private String name;
    @CsvBindByName
    private double quantity;
    @CsvBindByName
    private MaterialUnits unit;
    @CsvBindByName
    private double priceToBuy;
    @CsvBindByName
    private double priceToSell;

    public String[] attributesOfClassMaterial() {

        return new String[]{Long.toString(id),
                name,
                Double.toString(quantity),
                String.valueOf(unit),
                Double.toString(priceToBuy),
                Double.toString(priceToSell)};
    }
}
