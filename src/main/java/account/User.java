package account;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private long id;
    private String login;
    private String password;

    protected String[] attributesOfClass() {
        return new String[]{Long.toString(id), login, password};
    }

}
