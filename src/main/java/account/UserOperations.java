package account;

import CRUD.CRUD;

import lombok.Getter;
import lombok.Setter;

import java.io.File;

import java.util.Scanner;

@Getter
@Setter
public class UserOperations extends CRUD<User> {

    private final File usersDB = new File("src/main/resources/users.csv");
    public static boolean isLogged = false;
    private final Scanner sc = new Scanner(System.in);

    public void createAcc() {

        UserValidators uv = new UserValidators();

        System.out.println("Enter Login");
        String login = sc.nextLine();

        while (uv.checkInDB(login)) {
            System.out.println("Enter Login");
            login = sc.nextLine();
        }

        System.out.println("Enter Password");
        String password = sc.nextLine();

        while (!uv.checkStrengthOfPassword(password)) {

            System.out.println("Enter Password");
            password = sc.nextLine();
        }

        System.out.println("Account created properly!");

        long id = assignID(usersDB);

        User user = new User(id, login, password);
        save(usersDB, user.attributesOfClass());
    }

    public void login() {

        UserValidators uv = new UserValidators();

        System.out.println("Enter Login");
        String login = sc.next();

        System.out.println("Enter Password");
        String password = sc.next();

        if (uv.checkInDB(login, password))
            isLogged = true;
    }
}
