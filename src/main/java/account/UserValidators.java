package account;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidators extends UserOperations {

    public boolean checkInDB(String login, String password) {

        List<User> listOfUsers = findAll(getUsersDB(), User.class);
        for (User x : listOfUsers) {

            if (login.equals(x.getLogin()) && password.equals(x.getPassword())) {
                System.out.println("Logged in");
                return true;
            }
        }

        System.out.println("Incorrect Login or password");
        return false;
    }

    public boolean checkInDB(String login) {

        List<User> listOfUsers = findAll(getUsersDB(), User.class);
        for (User x : listOfUsers) {

            if (login.equals(x.getLogin())) {

                System.out.println("Login is already used");
                return true;
            }
        }

        return false;
    }

    public boolean checkStrengthOfPassword(String password) {

        Pattern compiledPattern = Pattern.compile("^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*\\W).{8,32}$");
        Matcher matcher = compiledPattern.matcher(password);

        if (matcher.matches()) {
            return true;
        } else {
            System.out.println("Password is too weak");
            return false;
        }

    }
}
