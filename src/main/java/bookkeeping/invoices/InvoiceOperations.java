package bookkeeping.invoices;

import CRUD.CRUD;
import bookkeeping.invoices.invoicePaymentCurrencies.InvoicesPaymentCurrencies;
import lombok.Getter;
import shop.cart.Cart;

import java.io.File;
import java.util.Arrays;
import java.util.Scanner;

@Getter
public class InvoiceOperations extends CRUD<Invoice> {

    private final File invoicesDB = new File("src/main/resources/invoices.csv");

    InvoiceValidators iv = new InvoiceValidators();

    Scanner sc = new Scanner(System.in);


    public void createInvoice(Cart cart, double amount) {

        System.out.println("Enter currency\n" + Arrays.toString(InvoicesPaymentCurrencies.values()));
        InvoicesPaymentCurrencies currency = InvoicesPaymentCurrencies.valueOf(sc.next());

        System.out.println("Enter payment way\n" + Arrays.toString(InvoicesPaymentWays.values()));
        InvoicesPaymentWays paymentWay = InvoicesPaymentWays.valueOf(sc.next());

        System.out.println("Enter contractor ID");
        long contractorId = sc.nextLong();

        while (!iv.checkIsContractorExists(contractorId)) {
            System.out.println("Enter contractor ID");
            contractorId = sc.nextLong();
        }

        long invoiceId = assignID(invoicesDB);

        Invoice invoice = new Invoice(invoiceId, amount, currency, paymentWay, contractorId, cart);

        save(invoicesDB, invoice.attributesOfClass());
    }

    private long getIdFromUser() {

        System.out.println("Enter ID");
        long id = sc.nextLong();
        sc.nextLine();

        return id;
    }

    public void displayById() {

        System.out.println(findById(getIdFromUser(), invoicesDB, Invoice.class));
    }

    public void deleteById() {

        deleteById(getIdFromUser(), invoicesDB);
    }

    public void updateImplementation() {

        System.out.println("Enter id of invoice to change");
        long id = sc.nextLong();
        sc.nextLine();

        Invoice invoiceToChange = findById(id, invoicesDB, Invoice.class);

        System.out.println("What would you like to change");
        System.out.println("1 - amount\n" +
                "2 - currency\n" +
                "3 - payment way\n" +
                "4 - contractor id");

        String choose = sc.next();

        switch (choose) {
            case "1" -> {
                System.out.println("Enter new amount");
                invoiceToChange.setAmount(sc.nextDouble());
            }
            case "2" -> {
                System.out.println("Enter new currency");
                System.out.println(Arrays.toString(InvoicesPaymentCurrencies.values()));
                invoiceToChange.setCurrency(InvoicesPaymentCurrencies.valueOf(sc.next()));
            }
            case "3" -> {
                System.out.println("Enter new payment way");
                System.out.println(Arrays.toString(InvoicesPaymentWays.values()));
                invoiceToChange.setPaymentWay(InvoicesPaymentWays.valueOf(sc.next()));
            }
            case "4" -> {
                System.out.println("Enter new contractor ID");
                long contractorId = sc.nextLong();

                while (!iv.checkIsContractorExists(contractorId)) {
                    System.out.println("Enter contractor ID");
                    contractorId = sc.nextLong();
                }
            }
            default -> System.out.println("Unknown command");
        }
        update(id, invoicesDB, invoiceToChange.attributesOfClass());
    }
}
