package bookkeeping.invoices.invoicePaymentCurrencies;

import currenciesRatesApi.Client;
import currenciesRatesApi.CurrencyModel;

import java.time.LocalDate;

public class InvoicePaymentCurrenciesOperations {


    public double valueInPln(String currencyCode, double amount)  {

        Client client = new Client();

        if (currencyCode.equals("PLN")) return amount;

        CurrencyModel cm = client.jsonToObject(currencyCode, getDate(currencyCode));

        amount = amount * cm.getRates().get(0).getAsk();

        return amount;
    }

    private String getDate(String currencyCode)  {

        LocalDate today = LocalDate.now();

        while (!isFound(currencyCode, today.toString())) {

            today = today.minusDays(1);

        }

        return today.toString();
    }

    private boolean isFound(String currencyCode, String date) {

        Client client = new Client();


        int statusCode = client.call(currencyCode, date).statusCode();


        return statusCode == 200;
    }
}
