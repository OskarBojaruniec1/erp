package bookkeeping.invoices.invoicePaymentCurrencies;

public enum InvoicesPaymentCurrencies {

    USD,
    EUR,
    PLN
}
