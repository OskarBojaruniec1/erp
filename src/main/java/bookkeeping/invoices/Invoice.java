package bookkeeping.invoices;

import bookkeeping.invoices.invoicePaymentCurrencies.InvoicesPaymentCurrencies;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvRecurse;
import lombok.*;
import shop.cart.Cart;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Invoice {

    @CsvBindByName
    private long id;
    @CsvBindByName
    private double amount;
    @CsvBindByName
    private InvoicesPaymentCurrencies currency;
    @CsvBindByName
    private InvoicesPaymentWays paymentWay;
    @CsvBindByName
    private long contractorId;
    @CsvRecurse
    private Cart cart;

    protected String[] attributesOfClass() {
        return new String[]{Long.toString(id),
                Double.toString(amount),
                String.valueOf(currency), String.valueOf(paymentWay),
                Long.toString(contractorId),
                cart.cartItemsAsString()
        };
    }
}
