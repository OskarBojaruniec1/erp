package bookkeeping.invoices;

public enum InvoicesPaymentWays {

    CASH,
    CARD,
    TRANSFER
}
