package bookkeeping.invoices;

import CRUD.CRUD;
import bookkeeping.contractor.Contractor;
import bookkeeping.contractor.ContractorOperations;

public class InvoiceValidators extends CRUD<Contractor> {

    public boolean checkIsContractorExists(long id) {
        ContractorOperations co = new ContractorOperations();

        if (findById(id, co.getContractorsDB(), Contractor.class) == null) {

            System.out.println("Contractor doesn't exists");
            return false;
        }

        return true;
    }
}
