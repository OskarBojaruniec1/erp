package bookkeeping.finances;

public class TaxesCalculator {

    public double calculateTax(double income) {

        double TAX_FREE = 30_000;
        double incomeWithOutTaxFree = income - TAX_FREE;

        if (incomeWithOutTaxFree > 0 && incomeWithOutTaxFree <= 120_000) {

            return calculateTaxFirstWay(incomeWithOutTaxFree);
        }

        if (incomeWithOutTaxFree > 120_000) {

            return calculateTaxSecondWay(incomeWithOutTaxFree);
        }

        return 0;
    }

    private double calculateTaxFirstWay(double incomeWithOutTaxFree) {
        return incomeWithOutTaxFree*0.17;
    }
    private double calculateTaxSecondWay(double incomeWithOutTaxFree){
        double tax = 120_000 * 0.17;

        tax += (incomeWithOutTaxFree - 120_000) * 0.32;

        return tax;
    }

}
