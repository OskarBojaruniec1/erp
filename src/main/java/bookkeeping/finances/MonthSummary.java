package bookkeeping.finances;

import bookkeeping.invoices.Invoice;
import bookkeeping.invoices.InvoiceOperations;
import bookkeeping.invoices.invoicePaymentCurrencies.InvoicePaymentCurrenciesOperations;
import shop.cart.Item;
import shop.cart.ItemOperations;
import staff.Employee;
import staff.EmployeeOperations;

import java.text.DecimalFormat;
import java.util.List;

public class MonthSummary {

    InvoiceOperations invoiceOperations = new InvoiceOperations();
    InvoicePaymentCurrenciesOperations invoicePaymentCurrenciesOperations =
            new InvoicePaymentCurrenciesOperations();

    EmployeeOperations employeeOperations = new EmployeeOperations();
    TaxesCalculator taxesCalculator = new TaxesCalculator();

    ItemOperations io = new ItemOperations();

    DecimalFormat df = new DecimalFormat("0.00");


    public void getTaxToPay() {

        double totalIncome = getTotalIncome();
        double employeesSalaries = calculateAllEmployeesSalaries();
        double costOfMaterials = calculateAllBoughtMaterials();

        double taxesToPay = taxesCalculator.calculateTax(totalIncome - employeesSalaries - costOfMaterials);
        System.out.println("Taxes to pay : " + df.format(taxesToPay));

    }

    private double calculateAllEmployeesSalaries() {

        List<Employee> employees = employeeOperations.findAll(employeeOperations.getEmployeesDB(),
                Employee.class);

        double salaries = 0;

        for (Employee employee : employees) {
            salaries += employee.getSalary();
        }

        System.out.println("Salaries of employees: " + df.format(salaries));
        return salaries;

    }

    private double getTotalIncome() {

        List<Invoice> invoices = invoiceOperations.findAll(invoiceOperations.getInvoicesDB(),
                Invoice.class);
        double income = 0;

        for (Invoice invoice : invoices) {
            income += calculateCurrencies(invoice);
        }

        System.out.println("Total income: " + df.format(income));
        return income;

    }

    private double calculateCurrencies(Invoice invoice) {

        double amount = invoice.getAmount();
        String currency = String.valueOf(invoice.getCurrency());

        return invoicePaymentCurrenciesOperations.valueInPln(currency, amount);
    }

    private double calculateAllBoughtMaterials(){

        List<Item> boughtMaterials = io.findAll(io.getItemsDB(),Item.class);

        double costOfBoughtMaterials = 0;
        for (Item i : boughtMaterials){

            costOfBoughtMaterials += i.getCost();
        }

        System.out.println("Cost of materials " +df.format(costOfBoughtMaterials));
        return costOfBoughtMaterials;
    }
}
