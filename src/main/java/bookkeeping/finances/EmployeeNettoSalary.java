package bookkeeping.finances;

import staff.Employee;
import staff.EmployeeOperations;

import java.text.DecimalFormat;
import java.util.List;

public class EmployeeNettoSalary {

    public static void calculateNettoSalary() {

        EmployeeOperations employeeOps = new EmployeeOperations();

        Employee employee = employeeOps.getById();

        double salary = employee.getSalary();

        double pensionContribution = salary * (9.76 / 100);
        double disabilityPensionContribution = salary * (1.50 / 100);
        double sicknessContribution = salary * (2.45 / 100);

        List<Double> contributions = List.of(pensionContribution,
                disabilityPensionContribution,
                sicknessContribution);

        for (Double x : contributions) {
            salary = salary - x;
        }

        double healthCareContribution = salary * (9.00 / 100);
        salary = salary - healthCareContribution;

        DecimalFormat df = new DecimalFormat("0.00");

        System.out.println("Pension contribution: " + df.format(pensionContribution));
        System.out.println("Disability pension contribution: " + df.format(disabilityPensionContribution));
        System.out.println("Sickness contribution: " + df.format(sicknessContribution));
        System.out.println("Health care contribution: " + df.format(healthCareContribution));

        System.out.println("Total netto salary: " + df.format(salary));


    }
}
