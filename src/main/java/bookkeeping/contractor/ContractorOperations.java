package bookkeeping.contractor;

import CRUD.CRUD;
import address.AddressOperations;
import lombok.Getter;

import java.io.File;
import java.util.Scanner;

@Getter
public class ContractorOperations extends CRUD<Contractor> {

    private final File contractorsDB = new File("src/main/resources/contractors.csv");

    ContractorValidators cv = new ContractorValidators();

    AddressOperations addressOp = new AddressOperations();

    Scanner sc = new Scanner(System.in);



    public void createContractor() {

        System.out.println("Enter name");
        String name = sc.next();

        System.out.println("Enter NIP");
        String nip = sc.next();

        while (!cv.checkIsNIPCorrect(nip)) {
            System.out.println("Enter NIP");
            nip = sc.next();
        }

        long id = assignID(contractorsDB);

        Contractor c = new Contractor(id, name, nip, addressOp.addAddress());

        save(contractorsDB, c.attributesOfClass());

    }

    private long getIdFromUser() {

        System.out.println("Enter ID");
        long id = sc.nextLong();
        sc.nextLine();

        return id;
    }

    public Contractor getByID() {

        return findById(getIdFromUser(), contractorsDB, Contractor.class);
    }

    public void deleteById() {

        deleteById(getIdFromUser(), contractorsDB);
    }

    public void updateImplementation() {


        System.out.println("Enter id of contractor to change");
        long id = sc.nextLong();
        sc.nextLine();

        Contractor contractorToChange = findById(id, contractorsDB, Contractor.class);

        System.out.println("What would you like to change");
        System.out.println("1 - Name\n" +
                "2 - NIP\n" +
                "3 - Address");

        String choose = sc.next();

        switch (choose) {
            case "1" -> {
                System.out.println("Enter new name");
                contractorToChange.setName(sc.next());
            }
            case "2" -> {
                System.out.println("Enter new nip");
                String nip = sc.next();

                while (!cv.checkIsNIPCorrect(nip)) {
                    System.out.println("Enter NIP");
                    nip = sc.next();
                }

                contractorToChange.setNip(nip);
            }
            case "3" -> contractorToChange.setAddress(addressOp.updateAddress(contractorToChange.getAddress()));

            default -> System.out.println("Unknown command");
        }
        update(id, contractorsDB, contractorToChange.attributesOfClass());
    }
}
