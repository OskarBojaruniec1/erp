package bookkeeping.contractor;

import address.Address;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvRecurse;
import lombok.*;
import org.apache.commons.lang3.ArrayUtils;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Contractor {

    @CsvBindByName
    private Long id;
    @CsvBindByName
    private String name;
    @CsvBindByName
    private String nip;
    @CsvRecurse
    private Address address;


    protected String[] attributesOfClass() {

        String[] attributes = {Long.toString(id), name, nip};

        return ArrayUtils.addAll(attributes, address.attributesOfClassAddress());

    }
}
