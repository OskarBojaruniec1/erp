package bookkeeping.contractor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContractorValidators {

    public boolean checkIsNIPCorrect(String nip) {

        String regex = "\\d{10}";

        Pattern compiledPattern = Pattern.compile(regex);
        Matcher matcher = compiledPattern.matcher(nip);

        if (matcher.matches()) {
            return true;
        } else {
            System.out.println("Nip is incorrect");
            return false;
        }
    }
}
