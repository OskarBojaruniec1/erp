package menus.staffMenu;

import staff.Employee;
import staff.EmployeeOperations;

import java.util.Scanner;

public class EmployeeMenu {

    private static void displayMenu() {

        System.out.println("\n1 - Add Employee\n" +
                "2 - Update Employee\n" +
                "3 - Display all Employees\n" +
                "4 - Display Employee by ID\n" +
                "5 - Delete Employee\n" +
                "0 - Back");

    }

    private static String declareOption() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Your choose: ");

        return sc.next();
    }

    public static void executeUserOption() {

        boolean inMethod = true;
        EmployeeOperations eo = new EmployeeOperations();

        while (inMethod) {

            displayMenu();

            String choose = declareOption();

            switch (choose) {

                case "1" -> eo.createEmployee();
                case "2" -> eo.updateImplementation();
                case "3" -> eo.findAll(eo.getEmployeesDB(), Employee.class).forEach(System.out::println);
                case "4" -> System.out.println(eo.getById());
                case "5" -> eo.deleteById();

                case "0" -> {
                    inMethod = false;
                    StaffMenu.executeUserOption();
                }
                default -> System.out.println("\nUnknown option\n");
            }
        }
    }
}
