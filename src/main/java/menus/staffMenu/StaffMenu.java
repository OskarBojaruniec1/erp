package menus.staffMenu;

import bookkeeping.finances.EmployeeNettoSalary;
import menus.MenuLogged;

import java.util.Scanner;

public class StaffMenu {

    private static void displayMenu() {

        System.out.println("\n1 - Employees\n" +
                "2 - Calculate salary\n" +
                "0 - Back");

    }

    private static String declareOption() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Your choose: ");

        return sc.next();
    }

    public static void executeUserOption() {

        boolean inMethod = true;

        while (inMethod) {

            displayMenu();

            String choose = declareOption();

            switch (choose) {

                case "1" -> EmployeeMenu.executeUserOption();
                case "2" -> EmployeeNettoSalary.calculateNettoSalary();

                case "0" -> {
                    inMethod = false;
                    MenuLogged.executeUserOption();
                }
                default -> System.out.println("\nUnknown option\n");
            }
        }
    }
}
