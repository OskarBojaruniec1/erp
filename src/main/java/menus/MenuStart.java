package menus;

import CRUD.CsvFiles;
import account.UserOperations;

import java.util.Scanner;

public class MenuStart {

    private static void displayMenu() {

        System.out.println("\nWelcome in your ERP");
        System.out.println("What do you want to do?");
        System.out.println("1 - Login\n" +
                "2 - Create account\n" +
                "3 - Exit");
    }

    private static String declareOption() {

        Scanner sc = new Scanner(System.in);
        System.out.print("Your choose: ");

        return sc.next();
    }

    public static void executeUserOption() {

        UserOperations userOp = new UserOperations();

        if(!userOp.getUsersDB().exists()){
            CsvFiles csvFiles = new CsvFiles();
            csvFiles.createCsvFiles();
        }

        while (!UserOperations.isLogged) {

            displayMenu();

            String choose = declareOption();

            switch (choose) {

                case "1" -> userOp.login();
                case "2" -> userOp.createAcc();
                case "3" -> System.exit(0);
                default -> System.out.println("\nUnknown option\n");
            }
        }

        MenuLogged.executeUserOption();
    }
}
