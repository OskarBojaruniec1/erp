package menus;

import account.UserOperations;
import menus.bookkeepingMenus.BookkeepingMenu;
import menus.staffMenu.StaffMenu;
import menus.warehouseMenus.WarehouseMenu;

import java.util.Scanner;

public class MenuLogged {

    private static void displayMenu() {

        System.out.println("\n1 - Bookkeeping\n" +
                "2 - Staff\n" +
                "3 - Warehouse\n" +
                "0 - Logout");

    }

    private static String declareOption() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Your choose: ");

        return sc.next();
    }

    public static void executeUserOption() {


        while (UserOperations.isLogged) {

            displayMenu();

            String choose = declareOption();

            switch (choose) {

                case "1" -> BookkeepingMenu.executeUserOption();
                case "2" -> StaffMenu.executeUserOption();
                case "3" -> WarehouseMenu.executeUserOption();
                case "0" -> {
                    UserOperations.isLogged = false;
                    MenuStart.executeUserOption();
                }
                default -> System.out.println("\nUnknown option\n");
            }
        }
    }
}
