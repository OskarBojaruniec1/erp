package menus.bookkeepingMenus;

import menus.MenuLogged;

import java.util.Scanner;

public class BookkeepingMenu {

    private static void displayMenu() {

        System.out.println("\n1 - Contractors\n" +
                "2 - Finances\n" +
                "3 - Invoices\n" +
                "0 - Back");

    }

    private static String declareOption() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Your choose: ");

        return sc.next();
    }

    public static void executeUserOption() {

        boolean inMethod = true;

        while (inMethod) {

            displayMenu();

            String choose = declareOption();

            switch (choose) {

                case "1" -> ContractorMenu.executeUserOption();
                case "2" -> FinancesMenu.executeUserOption();
                case "3" -> InvoicesMenu.executeUserOption();
                case "0" -> {
                    inMethod = false;
                    MenuLogged.executeUserOption();
                }
                default -> System.out.println("\nUnknown option\n");
            }
        }
    }
}
