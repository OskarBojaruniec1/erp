package menus.bookkeepingMenus;

import bookkeeping.invoices.Invoice;
import bookkeeping.invoices.InvoiceOperations;

import java.util.Scanner;

public class InvoicesMenu {

    private static void displayMenu() {

        System.out.println("1 - Update Invoice\n" +
                "2 - Display all Invoices\n" +
                "3 - Display Invoice by ID\n" +
                "4 - Delete Invoice\n" +
                "0 - Back");

    }

    private static String declareOption() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Your choose: ");

        return sc.next();
    }

    public static void executeUserOption() {

        boolean inMethod = true;
        InvoiceOperations io = new InvoiceOperations();

        while (inMethod) {

            displayMenu();

            String choose = declareOption();

            switch (choose) {

                case "1" -> io.updateImplementation();
                case "2" -> io.findAll(io.getInvoicesDB(), Invoice.class).forEach(System.out::println);
                case "3" -> io.displayById();
                case "4" -> io.deleteById();

                case "0" -> {
                    inMethod = false;
                    BookkeepingMenu.executeUserOption();
                }
                default -> System.out.println("\nUnknown option\n");
            }
        }
    }
}
