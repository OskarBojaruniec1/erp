package menus.bookkeepingMenus;

import bookkeeping.contractor.Contractor;
import bookkeeping.contractor.ContractorOperations;

import java.util.Scanner;

public class ContractorMenu {

    private static void displayMenu() {

        System.out.println("\n1 - Add Contractor\n" +
                "2 - Update Contractor\n" +
                "3 - Display all Contractors\n" +
                "4 - Display Contractor by ID\n" +
                "5 - Delete Contractor\n" +
                "0 - Back");

    }

    private static String declareOption() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Your choose: ");

        return sc.next();
    }

    public static void executeUserOption() {

        boolean inMethod = true;
        ContractorOperations co = new ContractorOperations();

        while (inMethod) {

            displayMenu();

            String choose = declareOption();

            switch (choose) {

                case "1" -> co.createContractor();
                case "2" -> co.updateImplementation();
                case "3" -> co.findAll(co.getContractorsDB(), Contractor.class).forEach(System.out::println);
                case "4" -> System.out.println(co.getByID());
                case "5" -> co.deleteById();
                case "0" -> {
                    inMethod = false;
                    BookkeepingMenu.executeUserOption();
                }
                default -> System.out.println("\nUnknown option\n");
            }
        }
    }
}
