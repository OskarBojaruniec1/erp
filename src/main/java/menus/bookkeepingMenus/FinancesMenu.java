package menus.bookkeepingMenus;

import bookkeeping.finances.MonthSummary;

import java.util.Scanner;

public class FinancesMenu {

    private static void displayMenu() {

        System.out.println("\n1 - Display month summary\n" +
                "0 - Back");

    }

    private static String declareOption() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Your choose: ");

        return sc.next();
    }

    public static void executeUserOption() {

        boolean inMethod = true;

        MonthSummary ms = new MonthSummary();


        while (inMethod) {

            displayMenu();

            String choose = declareOption();

            switch (choose) {

                case "1" -> ms.getTaxToPay();
                case "0" -> {
                    inMethod = false;
                    BookkeepingMenu.executeUserOption();
                }
                default -> System.out.println("\nUnknown option\n");
            }
        }
    }
}
