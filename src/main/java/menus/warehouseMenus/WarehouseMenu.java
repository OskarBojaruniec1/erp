package menus.warehouseMenus;

import menus.MenuLogged;
import shop.Shop;

import java.util.Scanner;

public class WarehouseMenu {


    private static void displayMenu() {

        System.out.println("\n1 - Materials\n" +
                "2 - Production\n" +
                "3 - Buy material\n" +
                "4 - Sell product\n" +
                "5 - Sell material\n" +
                "0 - Back");

    }

    private static String declareOption() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Your choose: ");

        return sc.next();
    }

    public static void executeUserOption() {

        boolean inMethod = true;
        Shop shop = new Shop();

        while (inMethod) {

            displayMenu();

            String choose = declareOption();

            switch (choose) {
                case "1" -> MaterialsMenu.executeUserOption();
                case "2" -> ProductionMenu.executeUserOption();
                case "3" -> shop.buyMaterials();
                case "4" -> shop.sellProduct();
                case "5" -> shop.sellMaterials();
                case "0" -> {
                    inMethod = false;
                    MenuLogged.executeUserOption();
                }
                default -> System.out.println("\nUnknown option\n");
            }
        }
    }
}