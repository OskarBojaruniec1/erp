package menus.warehouseMenus;


import warehouse.materials.Material;
import warehouse.materials.MaterialsOperations;

import java.util.Scanner;

public class MaterialsMenu {

    private static void displayMenu() {

        System.out.println("\n1 - Add new material\n" +
                "2 - Update material information\n" +
                "3 - Display all materials\n" +
                "4 - Display material by ID\n" +
                "5 - Delete material from warehouse\n" +
                "0 - Back");

    }

    private static String declareOption() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Your choose: ");

        return sc.next();
    }

    public static void executeUserOption() {

        boolean inMethod = true;
        MaterialsOperations mo = new MaterialsOperations();

        while (inMethod) {

            displayMenu();

            String choose = declareOption();

            switch (choose) {

                case "1" -> mo.addNewMaterial();
                case "2" -> mo.updateImplementation();
                case "3" -> mo.findAll(mo.getWarehouseDB(), Material.class).forEach(System.out::println);
                case "4" -> System.out.println(mo.getById());
                case "5" -> mo.deleteById();

                case "0" -> {
                    inMethod = false;
                    WarehouseMenu.executeUserOption();
                }
                default -> System.out.println("\nUnknown option\n");
            }
        }
    }
}
