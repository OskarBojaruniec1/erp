package menus.warehouseMenus;

import warehouse.production.recipes.Recipe;
import warehouse.production.recipes.RecipeOperations;

import java.util.Scanner;

public class ProductionMenu {

    private static void displayMenu() {

        System.out.println("\n1 - Add new model\n" +
                "2 - Update recipe of model\n" +
                "3 - Display all models\n" +
                "4 - Display model by ID\n" +
                "5 - Delete model from warehouse\n" +
                "0 - Back");

    }

    private static String declareOption() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Your choose: ");

        return sc.next();
    }

    public static void executeUserOption() {

        boolean inMethod = true;
        RecipeOperations ro = new RecipeOperations();

        while (inMethod) {

            displayMenu();

            String choose = declareOption();

            switch (choose) {

                case "1" -> ro.createNewRecipe();
                case "2" -> ro.updateImplementation();
                case "3" -> ro.findAll(ro.getRecipesDB(), Recipe.class).forEach(System.out::println);
                case "4" -> System.out.println(ro.getById());
                case "5" -> ro.deleteById();

                case "0" -> {
                    inMethod = false;
                    WarehouseMenu.executeUserOption();
                }
                default -> System.out.println("\nUnknown option\n");
            }
        }
    }
}
